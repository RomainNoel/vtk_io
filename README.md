# VTK_IO

A single file libary for VTK Input/Output in Fortran.

This module is largely adapted from [VTKFortran](https://github.com/szaghi/VTKFortran).


## Requirements

* [PorPre](https://gitlab.com/RomainNoel/porpre) a portable precision library
* [BeFor_64](https://gitlab.com/RomainNoel/BeFor_64) a Fortran log library
