!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -*- Mode: F90 -*- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! VTK_mod.f90 --- VTK data file format
!!
!! Autor           : Romain NOEL (EMSE) <romain.noel@emse.fr>
!! Created         : Tue Jan 10 14:35 2017
!! last. mod. by   : Romain NOEL (EMSE) <romain.noel@emse.fr>
!! last. mod. on   : Wed Jan 11 14:01 2017
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

MODULE VTK_IO
   USE PorPre,  ONLY: vsi, di, sr, dr, endianess
   implicit none

   Private

   integer, parameter          :: s=sr
   integer, parameter          :: d=dr
   character(len=1), parameter :: newline=char(10)
   integer                     :: iproc=0, nb_procs=1
   !----------------------------------------------------------------------------

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!   VTK subtype   !!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! GeometyVTK
   type, abstract :: GeometyVTK
      character(len=20) :: XMLend
   contains
      procedure(VTK_write_mesh_3d_npspor), nopass, deferred :: write_mesh_3d_npspor
      procedure(VTK_write_mesh_3d_xyz), nopass, deferred :: write_mesh_3d_xyz
   end type GeometyVTK

   ! type, extends(GeometyVTK) :: GeometyPolygonal
   ! contains
   ! end type GeometyPolygonal

   ! type, extends(GeometyVTK) :: GeometyUnstructured

   ! end type GeometyUnstructured

   ! type, extends(GeometyVTK) :: GeometyStructured
   ! contains
   ! end type GeometyStructured

   ! type, extends(GeometyVTK) :: GeometyRectilinear
   ! contains
   ! end type GeometyRectilinear

   type, extends(GeometyVTK) :: GeometyRectilinear_leg_ascii
   contains
      procedure, nopass :: write_mesh_3d_npspor => write_mesh_3d_dumb_npspor
      procedure, nopass :: write_mesh_3d_xyz => write_mesh_3d_leg_ascii_rect_xyz
   end type GeometyRectilinear_leg_ascii

   type, extends(GeometyVTK) :: GeometyRectilinear_leg_bin
   contains
      procedure, nopass :: write_mesh_3d_npspor => write_mesh_3d_dumb_npspor
      procedure, nopass :: write_mesh_3d_xyz => write_mesh_3d_leg_bin_rect_xyz
   end type GeometyRectilinear_leg_bin

   type, extends(GeometyVTK) :: GeometyRectilinear_XML_ascii
   contains
      procedure, nopass :: write_mesh_3d_npspor => write_mesh_3d_dumb_npspor
      procedure, nopass :: write_mesh_3d_xyz => write_mesh_3d_XML_ascii_rect_xyz
   end type GeometyRectilinear_XML_ascii

   type, extends(GeometyVTK) :: GeometyRectilinear_XML_bin
   contains
      procedure, nopass :: write_mesh_3d_npspor => write_mesh_3d_dumb_npspor
      procedure, nopass :: write_mesh_3d_xyz => write_mesh_3d_XML_bin_rect_xyz
   end type GeometyRectilinear_XML_bin

   ! type, extends(GeometyVTK) :: GeometyImagePoint
   ! contains
   ! end type GeometyImagePoint

   type, extends(GeometyVTK) :: GeometyImagePoint_leg_ascii
   contains
      procedure, nopass :: write_mesh_3d_npspor => write_mesh_3d_leg_ascii_ima_npspor
      procedure, nopass :: write_mesh_3d_xyz => write_mesh_3d_dumb_xyz
   end type GeometyImagePoint_leg_ascii

   type, extends(GeometyVTK) :: GeometyImagePoint_leg_bin
   contains
      procedure, nopass :: write_mesh_3d_npspor => write_mesh_3d_leg_bin_ima_npspor
      procedure, nopass :: write_mesh_3d_xyz => write_mesh_3d_dumb_xyz
   end type GeometyImagePoint_leg_bin

   type, extends(GeometyVTK) :: GeometyImagePoint_XML_ascii
   contains
      procedure, nopass :: write_mesh_3d_npspor => write_mesh_3d_XML_ascii_ima_npspor
      procedure, nopass :: write_mesh_3d_xyz => write_mesh_3d_dumb_xyz
   end type GeometyImagePoint_XML_ascii

   type, extends(GeometyVTK) :: GeometyImagePoint_XML_bin
   contains
      procedure, nopass :: write_mesh_3d_npspor => write_mesh_3d_XML_bin_ima_npspor
      procedure, nopass :: write_mesh_3d_xyz => write_mesh_3d_dumb_xyz
   end type GeometyImagePoint_XML_bin

  ! Format
   type, abstract :: FormatVTK
      class(GeometyVTK), allocatable:: geom
      character(len=6)           :: endian
   contains
      procedure(VTK_open_file),nopass, deferred :: open_files
      procedure(VTK_close_file),nopass, deferred :: close_files
      procedure(VTK_write_scalar3d_1r3_dr),nopass, deferred :: write_scalar3d_1r3_dr
      procedure(VTK_write_array3d_3r3_dr),nopass, deferred :: write_array3d_3r3_dr
      procedure(VTK_write_array3d_1r4_dr),nopass, deferred :: write_array3d_1r4_dr
      procedure(VTK_write_matrix3d_1r5_dr),nopass, deferred :: write_matrix3d_1r5_dr
      procedure(VTK_write_matrix3d_9r3_dr),nopass, deferred :: write_matrix3d_9r3_dr
      procedure(VTK_write_matrix3d_3r4_dr),nopass, deferred :: write_matrix3d_3r4_dr
   end type FormatVTK

   type, abstract, extends(FormatVTK) :: FormatVTK_leg
   contains
   end type FormatVTK_leg

   type, abstract, extends(FormatVTK) :: FormatVTK_XML
   contains
   end type FormatVTK_XML

   ! type, extends(FormatVTK) :: FormatRaw
   !    character(len=6)           :: endian
   !    character(len=6)     :: compressor
   ! contains
   ! end type FormatRaw

   ! type, extends(FormatVTK) :: FormatAppend
   !    character(len=6)           :: endian
   !    character(len=6)     :: compressor
   ! contains
   ! end type FormatAppend

   type, extends(FormatVTK_leg) :: FormatAscii_leg
   contains
      procedure, nopass :: open_files => open_file_leg_ascii
      procedure, nopass :: close_files => close_file_leg_ascii
      procedure, nopass :: write_scalar3d_1r3_dr => write_scalar3d_1r3_dr_leg_ascii
      procedure, nopass :: write_array3d_3r3_dr => write_array3d_3r3_dr_leg_ascii
      procedure, nopass :: write_array3d_1r4_dr => write_array3d_1r4_dr_leg_ascii
      procedure, nopass :: write_matrix3d_1r5_dr => write_matrix3d_1r5_dr_leg_ascii
      procedure, nopass :: write_matrix3d_9r3_dr => write_matrix3d_9r3_dr_leg_ascii
      procedure, nopass :: write_matrix3d_3r4_dr => write_matrix3d_3r4_dr_leg_ascii
   end type FormatAscii_leg

   type, extends(FormatVTK_leg) :: FormatBinary_leg
      ! character(len=6)           :: endian
   contains
      procedure, nopass :: open_files => open_file_leg_bin
      procedure, nopass :: close_files => close_file_leg_bin
      procedure, nopass :: write_scalar3d_1r3_dr => write_scalar3d_1r3_dr_leg_bin
      procedure, nopass :: write_array3d_3r3_dr => write_array3d_3r3_dr_leg_bin
      procedure, nopass :: write_array3d_1r4_dr => write_array3d_1r4_dr_leg_bin
      procedure, nopass :: write_matrix3d_1r5_dr => write_matrix3d_1r5_dr_leg_bin
      procedure, nopass :: write_matrix3d_9r3_dr => write_matrix3d_9r3_dr_leg_bin
      procedure, nopass :: write_matrix3d_3r4_dr => write_matrix3d_3r4_dr_leg_bin
   end type FormatBinary_leg

   type, extends(FormatVTK_XML) :: FormatAscii_XML
   contains
      procedure, nopass :: open_files => open_file_XML_ascii
      procedure, nopass :: close_files => close_file_XML_ascii
      procedure, nopass :: write_scalar3d_1r3_dr => write_scalar3d_1r3_dr_XML_ascii
      procedure, nopass :: write_array3d_3r3_dr => write_array3d_3r3_dr_XML_ascii
      procedure, nopass :: write_array3d_1r4_dr => write_array3d_1r4_dr_XML_ascii
      procedure, nopass :: write_matrix3d_1r5_dr => write_matrix3d_1r5_dr_XML_ascii
      procedure, nopass :: write_matrix3d_9r3_dr => write_matrix3d_9r3_dr_XML_ascii
      procedure, nopass :: write_matrix3d_3r4_dr => write_matrix3d_3r4_dr_XML_ascii
   end type FormatAscii_XML

   type, extends(FormatVTK_XML) :: FormatBinary_XML
      ! character(len=6)           :: endian
   contains
      procedure, nopass :: open_files => open_file_XML_bin
      procedure, nopass :: close_files => close_file_XML_bin
      procedure, nopass :: write_scalar3d_1r3_dr => write_scalar3d_1r3_dr_XML_bin
      procedure, nopass :: write_array3d_3r3_dr => write_array3d_3r3_dr_XML_bin
      procedure, nopass :: write_array3d_1r4_dr => write_array3d_1r4_dr_XML_bin
      procedure, nopass :: write_matrix3d_1r5_dr => write_matrix3d_1r5_dr_XML_bin
      procedure, nopass :: write_matrix3d_9r3_dr => write_matrix3d_9r3_dr_XML_bin
      procedure, nopass :: write_matrix3d_3r4_dr => write_matrix3d_3r4_dr_XML_bin
   end type FormatBinary_XML

   ! TypeVTK
   type, abstract :: TypeVTK
      character(len=6)           :: version
      class(FormatVTK), allocatable :: format
   end type TypeVTK

   type, extends(TypeVTK) :: TypeLegacy
      !character(len=6)           :: version
   contains
   end type TypeLegacy

   type, extends(TypeVTK) :: TypeXML
   contains
   end type TypeXML

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!   VTK maintype  !!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! VTKfileHandler
   type,  public      :: VTKfileHandler
      private
      character(len=80)          :: prefix
      integer                    :: unit
      integer                    :: ni, nj, nk
      character(len=80)          :: mesh_topology
      integer                    :: counter=0
      integer                    :: restart=0
      logical                    :: first=.true.
      character(len=10)          :: format
      character(len=10), public  :: type_string
      character(len=6)           :: ext
      character(len=:), allocatable  :: indent
      class(TypeVTK), allocatable    :: type
      ! procedure(), pointer, nopass :: VTK_write_mesh_3d_b
      contains
         ! procedure(VTK_write_mesh_2d),   pass(fd), deferred        :: VTK_write_mesh_2d
         ! procedure(VTK_write_mesh_3d),   pass(fd), deferred        :: VTK_write_mesh_3d
         ! procedure(VTK_write_scalar_2d), pass(fd), deferred        :: VTK_write_scalar_2d
         ! procedure(VTK_write_scalar_3d), pass(fd), deferred        :: VTK_write_scalar_3d
         ! procedure(VTK_write_vector_2d), pass(fd), deferred        :: VTK_write_vector_2d
         ! procedure(VTK_write_vector_3d), pass(fd), deferred        :: VTK_write_vector_3d
         ! generic, public :: write_mesh => VTK_write_mesh_2d, VTK_write_mesh_3d
         ! generic, public :: write_var  => VTK_write_scalar_2d, VTK_write_scalar_3d, &
         !                               &  VTK_write_vector_2d, VTK_write_vector_3d
         ! procedure(VTK_write_time),    nopass  , deferred, public  :: write_time
         ! procedure(VTK_open_file),     pass(fd), deferred, public  :: open_file
         ! procedure(VTK_close_file),    pass(fd), deferred, public  :: close_file
         ! ! procedure(VTK_collect_file),  pass(fd), public  :: collect_file => VTK_collect_file_gen

         procedure,  pass(fd), public  :: open_file
         procedure, pass(fd), public :: close_file
         procedure,  pass(fd), public  :: collect_file => VTK_collect_file_gen
         procedure,pass(fd), private  :: write_mesh_3d_npspor
         procedure,pass(fd), private  :: write_mesh_3d_xyz
         generic, public  :: write_mesh => write_mesh_3d_npspor, write_mesh_3d_xyz
         procedure,pass(fd), private  :: write_scalar3d_1r3_dr
         procedure,pass(fd), private  :: write_array3d_3r3_dr
         procedure,pass(fd), private  :: write_array3d_1r4_dr
         procedure,pass(fd), private  :: write_matrix3d_1r5_dr
         procedure,pass(fd), private  :: write_matrix3d_9r3_dr
         procedure,pass(fd), private  :: write_matrix3d_3r4_dr
         generic, public  :: write_data => write_scalar3d_1r3_dr, write_array3d_3r3_dr, write_array3d_1r4_dr, &
                           & write_matrix3d_1r5_dr, write_matrix3d_9r3_dr, write_matrix3d_3r4_dr
         procedure,pass(fd), private  :: write_color_dr
         generic, public  :: write_color => write_color_dr
         procedure,pass(fd), private  :: write_normal_dr
         generic, public  :: write_normal => write_normal_dr
         procedure,pass(fd), private  :: write_texture_dr
         generic, public  :: write_texture => write_texture_dr
   end type VTKfileHandler


   ! private :: VTK_write_mesh_2d,   VTK_write_mesh_3d,   &   ! not necessary because l14 : private (implicitely everything is private)
   !            VTK_write_vector_2d, VTK_write_vector_3d, &
   !            VTK_write_scalar_2d, VTK_write_scalar_3d

   ! ! Make public some elements (variables, subroutines, function) from
   ! this module or its descendentes.
   public :: VTK_init_file, VTK_dealloc, &
            VTK_read_file

   ! interface VTK_write_mesh
   !   module procedure VTK_write_mesh_2d, VTK_write_mesh_3d
   ! end interface


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!    Interfaces          !!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   abstract interface
      subroutine VTK_open_file( fd, proc_rank, num_procs, restart )
         !USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(inout) :: fd
         integer, optional, intent(in)         :: proc_rank, num_procs, restart
      end subroutine
      subroutine VTK_close_file(fd)
         import VTKfileHandler
         class(VTKfileHandler), intent(in)    :: fd
      end subroutine
      subroutine VTK_collect_file(fd)
         ! USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)    :: fd
      end subroutine
      subroutine VTK_write_time(fd, t, doubled)
         !USE PorPre, only: dr
         import VTKfileHandler
         implicit none
         class(VTKfileHandler), intent(in)    :: fd
         integer , intent(in)                     :: t
         logical, optional, intent(in)           :: doubled
      end subroutine
   end interface

   abstract interface
      subroutine VTK_write_mesh_2d(fd, x, y)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(inout)    :: fd
         real(kind=dr), intent(in), dimension(:)   :: x, y
      end subroutine
      subroutine VTK_write_mesh_3d_xyz(fd, x, y, z)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(inout)    :: fd
         real(kind=dr), intent(in), dimension(:)   :: x, y, z
      end subroutine
      subroutine VTK_write_mesh_3d_npspor(fd, np, space, origin)
         USE PorPre, only: di, dr
         import VTKfileHandler
         class(VTKfileHandler), intent(inout)     :: fd
         integer(kind=di), intent(in) :: np(1:3)
         real(kind=dr), intent(in)  :: space(1:3)
         real(kind=dr), intent(in)  :: origin(1:3)
      end subroutine
      subroutine VTK_write_mesh_4d(fd, x, y, z, t)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(inout)    :: fd
         real(kind=dr), intent(in), dimension(:)   :: x, y, z, t
      end subroutine
   end interface

   abstract interface
      subroutine VTK_write_scalar_2d(fd, name, field)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in), dimension(:,:)  :: field
      end subroutine
      subroutine VTK_write_scalar_3d(fd, name, field)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in), dimension(:,:,:)  :: field
      end subroutine
      subroutine VTK_write_vector_2d(fd, name, vx, vy)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in), dimension(:,:)  :: vx, vy
      end subroutine
      subroutine VTK_write_scalar3d_1r3_dr(fd, name, s)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in), dimension(:,:,:)  :: s
      end subroutine
      subroutine VTK_write_array3d_3r3_dr(fd, name, vx, vy, vz)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in), dimension(:,:,:) :: vx, vy, vz
      end subroutine
      subroutine VTK_write_array3d_1r4_dr(fd, name, v)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in)  :: v(:,:,:,:)
      end subroutine
      subroutine VTK_write_matrix3d_1r5_dr(fd, name, m)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in), dimension(:,:,:,:,:)  :: m
      end subroutine
      subroutine VTK_write_matrix3d_9r3_dr(fd, name, mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in), dimension(:,:,:)  :: mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz
      end subroutine
      subroutine VTK_write_matrix3d_3r4_dr(fd, name, vx, vy, vz)
         USE PorPre, only: dr
         import VTKfileHandler
         class(VTKfileHandler), intent(in)  :: fd
         character(len=*), intent(in)  :: name
         real(kind=dr), intent(in), dimension(:,:,:,:)  :: vx, vy, vz
      end subroutine
   end interface

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!    VTK extensions of the main type  !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


contains

   subroutine VTK_init_file(fd, prefix, mesh_topo, format, types, endian)
      !============================================================================
      !     VTK_init_file to allocate the right type
      !============================================================================
      implicit none

      class(VTKfileHandler), intent(inout)    :: fd
      character(len=*),                    intent(in)       :: prefix
      character(len=*),                    intent(in)       :: mesh_topo
      character(len=*),                    intent(in)       :: format
      character(len=*),                    intent(in)       :: types
      character(len=*),       optional,    intent(in)       :: endian
      !----------------------------------------------------------------------------

      if (types=='legacy') then
         allocate(TypeLegacy :: fd%type)
         fd%type_string=types
         fd%ext='.vtk'
         fd%type%version='3.0'
         if (format=='ascii') then
            allocate( FormatAscii_leg :: fd%type%format)
            fd%type%format%endian='None'
            if (mesh_topo=='image' .or. mesh_topo=='points') then
               allocate( GeometyImagePoint_leg_ascii :: fd%type%format%geom)
            elseif (mesh_topo=='rectilinear') then
               allocate( GeometyRectilinear_leg_ascii :: fd%type%format%geom)
            ! elseif (mesh_topo=='structured') then
            !    allocate( GeometyStructured_leg_ascii :: fd%type%format%geom)
            ! elseif (mesh_topo=='unstructured') then
            !    allocate( GeometyUnstructured_leg_ascii :: fd%type%format%geom)
            ! elseif (mesh_topo=='polygonal') then
            !    allocate( GeometyPolygonal_leg_ascii :: fd%type%format%geom)
            else
               print*, 'Error in VTK_init_file with the combinaison :',mesh_topo//', ', types//', ', format
               ! ERROR STOP 6661    Not currently supported by PGI
               STOP 6661
            endif
         elseif (format=='binary') then
            allocate( FormatBinary_leg :: fd%type%format)
            if ( present(endian) ) then
               fd%type%format%endian=trim(adjustl(endian))
            else
               fd%type%format%endian=trim(adjustl(endianess)) ! from PorPre
            endif
            if (mesh_topo=='image' .or. mesh_topo=='points') then
               allocate( GeometyImagePoint_leg_bin :: fd%type%format%geom)
            elseif (mesh_topo=='rectilinear') then
               allocate( GeometyRectilinear_leg_bin :: fd%type%format%geom)
            ! elseif (mesh_topo=='structured') then
            !    allocate( GeometyStructured_leg_bin :: fd%type%format%geom)
            ! elseif (mesh_topo=='unstructured') then
            !    allocate( GeometyUnstructured_leg_bin :: fd%type%format%geom)
            ! elseif (mesh_topo=='polygonal') then
            !    allocate( GeometyPolygonal_leg_bin :: fd%type%format%geom)
            else
               print*, 'Error in VTK_init_file with the combinaison :',mesh_topo//', ', types//', ', format
               ! ERROR STOP 6662    Not currently supported by PGI
               STOP 6662
            endif
         else
            print*, 'Error in VTK_init_file with the combinaison :',mesh_topo//', ', types//', ', format
            ! ERROR STOP 6663    Not currently supported by PGI
            STOP 6663
         endif
         fd%type%format%geom%XMLend='None'
      elseif (types=='XML') then
         allocate(TypeXML :: fd%type)
         fd%type_string=types
         fd%type%version='0.1'
         if (format=='ascii') then
            allocate( FormatAscii_XML :: fd%type%format)
            fd%type%format%endian='None'
            if (mesh_topo=='image' .or. mesh_topo=='points') then
               allocate( GeometyImagePoint_XML_ascii :: fd%type%format%geom)
               fd%ext='.vti'
            elseif (mesh_topo=='rectilinear') then
               allocate( GeometyRectilinear_XML_ascii :: fd%type%format%geom)
               fd%ext='.vtr'
            ! elseif (mesh_topo=='structured') then
            !    allocate( GeometyStructured_XML_ascii :: fd%type%format%geom)
            !    fd%ext='.vts'
            ! elseif (mesh_topo=='unstructured') then
            !    allocate( GeometyUnstructured_XML_ascii :: fd%type%format%geom)
            !    fd%ext='.vtu'
            ! elseif (mesh_topo=='polygonal') then
            !    allocate( GeometyPolygonal_XML_ascii :: fd%type%format%geom)
            !    fd%ext='.vtp'
            else
               print*, 'Error in VTK_init_file with the combinaison :',mesh_topo//', ', types//', ', format
               ! ERROR STOP 6664    Not currently supported by PGI
               STOP 6664
            endif
         elseif (format=='binary') then
            allocate( FormatBinary_XML :: fd%type%format)
            if ( present(endian) ) then
               fd%type%format%endian=trim(adjustl(endian))
            else
               fd%type%format%endian=trim(adjustl(endianess)) ! from PorPre
            endif
            if (mesh_topo=='image' .or. mesh_topo=='points') then
               allocate( GeometyImagePoint_XML_bin :: fd%type%format%geom)
               fd%ext='.vti'
            elseif (mesh_topo=='rectilinear') then
               allocate( GeometyRectilinear_XML_bin :: fd%type%format%geom)
               fd%ext='.vtr'
            ! elseif (mesh_topo=='structured') then
            !    allocate( GeometyStructured_XML_bin :: fd%type%format%geom)
            !    fd%ext='.vts'
            ! elseif (mesh_topo=='unstructured') then
            !    allocate( GeometyUnstructured_XML_bin :: fd%type%format%geom)
            !    fd%ext='.vtu'
            ! elseif (mesh_topo=='polygonal') then
            !    allocate( GeometyPolygonal_XML_bin :: fd%type%format%geom)
            !    fd%ext='.vtp'
            else
               print*, 'Error in VTK_init_file with the combinaison :',mesh_topo//', ', types//', ', format
               ! ERROR STOP 6665    Not currently supported by PGI
               STOP 6665
            endif

         ! elseif (format='raw') then
         !    allocate( FormatRaw_XML :: fd%type%format)
         ! elseif (format='append') then
         !    allocate( FormatAppend_XML :: fd%type%format)
         else
            print*, 'Error in VTK_init_file with the combinaison :',mesh_topo//', ', types//', ', format
            ! ERROR STOP 6667    Not currently supported by PGI
            STOP 6667
         endif
      else
         print*, 'Error in VTK_init_file with the combinaison :',mesh_topo//', ', types//', ', format
         ! ERROR STOP 6668    Not currently supported by PGI
         STOP 6668
      endif

      fd%prefix=trim(prefix)
      fd%mesh_topology=trim(mesh_topo)
      fd%format=trim(format)
   end subroutine VTK_init_file

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!    FORMAT Functions: Open, Close, Write_Scalar, Write_Array ...    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! VTKfileHandler
   subroutine open_file( fd, proc_rank, num_procs, restart )
      !=========================================================================
      !
      !=========================================================================
      implicit none

      integer, optional, intent(in)         :: proc_rank, num_procs, restart
      class(VTKfileHandler), intent(inout) :: fd
      !-------------------------------------------------------------------------

      ! open the file with the correct VTK type and format.
      call fd%type%format%open_files(fd, proc_rank, num_procs, restart)
   end subroutine open_file


   subroutine close_file(fd)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      !-------------------------------------------------------------------------

      ! Close the file with the correct VTK type and format.
      call fd%type%format%close_files(fd)
   end subroutine close_file


   subroutine VTK_dealloc(fd)
      !=========================================================================
      ! deallocate the allocated values in the VTKfileHandler.
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout) :: fd
      !-------------------------------------------------------------------------

      ! Liberate memory space.
      if (allocated(fd%indent)) deallocate(fd%indent)
      if (allocated(fd%type)) deallocate(fd%type)
   end subroutine VTK_dealloc


   subroutine write_scalar3d_1r3_dr(fd, name, s)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real (kind=dr), intent(in)  :: s(:,:,:)
      !-------------------------------------------------------------------------

      ! Close the file with the correct VTK type and format.
      call fd%type%format%write_scalar3d_1r3_dr(fd, name, s)
   end subroutine write_scalar3d_1r3_dr

   subroutine write_array3d_1r4_dr(fd, name, v)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in) :: v(:,:,:,:)
      !-------------------------------------------------------------------------

      ! Close the file with the correct VTK type and format.
      call fd%type%format%write_array3d_1r4_dr(fd, name, v)
   end subroutine write_array3d_1r4_dr

   subroutine write_array3d_3r3_dr(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:) :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      call fd%type%format%write_array3d_3r3_dr(fd, name, vx, vy, vz)
   end subroutine write_array3d_3r3_dr

   subroutine write_matrix3d_1r5_dr(fd, name, m)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:,:,:) :: m
      !-------------------------------------------------------------------------

      !
      call fd%type%format%write_matrix3d_1r5_dr(fd, name, m)
   end subroutine write_matrix3d_1r5_dr

      subroutine write_matrix3d_9r3_dr(fd, name, mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:) :: mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz
      !-------------------------------------------------------------------------

      !
      call fd%type%format%write_matrix3d_9r3_dr(fd, name, mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz)
   end subroutine write_matrix3d_9r3_dr

      subroutine write_matrix3d_3r4_dr(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:,:) :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      call fd%type%format%write_matrix3d_3r4_dr(fd, name, vx, vy, vz)
   end subroutine write_matrix3d_3r4_dr

   subroutine write_color_dr(fd, name )
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      ! real (kind=dr), intent(in), dimension(:,:,:) :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_color_dr not yet implemented'
   end subroutine write_color_dr

   subroutine write_normal_dr(fd, name )
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      ! real (kind=dr), intent(in), dimension(:,:,:) :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_normal_dr not yet implemented'
   end subroutine write_normal_dr

   subroutine write_texture_dr(fd, name )
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      ! real (kind=dr), intent(in), dimension(:,:,:) :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_texture_dr not yet implemented'
   end subroutine write_texture_dr

   ! FORMAT
   subroutine open_file_leg_ascii( fd, proc_rank, num_procs, restart )
      !=========================================================================
      !
      !=========================================================================
      implicit none

      integer, optional, intent(in)         :: proc_rank, num_procs, restart
      class(VTKfileHandler), intent(inout) :: fd
      character(len=10)                     :: rank, snapshot
      character(len=80)                     :: f
      ! character(len=80)                     :: char_mesh
      ! character(len=256)                    :: MAIN_header
      integer                               :: ios
      !-------------------------------------------------------------------------

      if ( present(proc_rank) .and. present(num_procs) ) then

         iproc    = proc_rank
         nb_procs = num_procs

      else if ( present(proc_rank) ) then
         print *,"[ERROR]     ","VTK_open_file","Both PROC_RANK and NUM_PROCS arguments must be present."
         stop
      else if ( present(num_procs) ) then
         print *,"[ERROR]     ","VTK_open_file","Both PROC_RANK and NUM_PROCS arguments must be present."
         stop
      end if

      if ((fd%first) .and. (present(restart))) then
         fd%restart=restart
         fd%counter=restart
         fd%first=.false.
      end if

      fd%counter=fd%counter+1
      write(snapshot,'(i8)') fd%counter

      if ( present(proc_rank) ) then
         write(rank,'(i8)') iproc
         fd%ext='.p'//fd%ext(1:4)
         f=trim(fd%prefix)//"_"//trim(adjustl(rank))//"_"//trim(adjustl(snapshot))//fd%ext
      else
         f=trim(fd%prefix)//"_"//trim(adjustl(snapshot))//fd%ext
      end if

      open(newunit    = fd%unit,          &
         file       = trim(adjustl(f)), &
         form       = 'FORMATTED',      & ! FORMATTED
         access     = 'STREAM',         & ! SEQUENTIAL
         action     = 'WRITE',          &
         status     = 'REPLACE',        &
         iostat     = ios)

      if(ios /= 0) print '("Problem creating file ",a,".")', trim(f)

      write(unit=fd%unit,fmt='(100A)') "# vtk DataFile Version "//fd%type%version//newline//'VTK '//trim(fd%prefix)&
                                       &//newline//fd%format
   end subroutine open_file_leg_ascii

   subroutine open_file_leg_bin( fd, proc_rank, num_procs, restart )
      !=========================================================================
      !
      !=========================================================================
      implicit none

      integer, optional, intent(in)         :: proc_rank, num_procs, restart
      class(VTKfileHandler), intent(inout) :: fd
      character(len=10)                     :: rank, snapshot
      character(len=80)                     :: f
      ! character(len=80)                     :: char_mesh
      ! character(len=256)                    :: MAIN_header
      integer                               :: ios
      !-------------------------------------------------------------------------
 
      !
      if ( present(proc_rank) .and. present(num_procs) ) then
         iproc    = proc_rank
         nb_procs = num_procs
      else if ( present(proc_rank) ) then
         print *,"[ERROR]     ","VTK_open_file","Both PROC_RANK and NUM_PROCS arguments must be present."
         stop
      else if ( present(num_procs) ) then
         print *,"[ERROR]     ","VTK_open_file","Both PROC_RANK and NUM_PROCS arguments must be present."
         stop
      end if

      if ((fd%first) .and. (present(restart))) then
         fd%restart=restart
         fd%counter=restart
         fd%first=.false.
      end if

      fd%counter=fd%counter+1
      write(snapshot,'(i8)') fd%counter

      if ( present(proc_rank) ) then
         write(rank,'(i8)') iproc
         fd%ext='.p'//fd%ext(1:4)
         f=trim(fd%prefix)//"_"//trim(adjustl(rank))//"_"//trim(adjustl(snapshot))//fd%ext
      else
         f=trim(fd%prefix)//"_"//trim(adjustl(snapshot))//fd%ext
      end if

      open(newunit    = fd%unit,                     &
          file       = trim(adjustl(f)),            &
          form       = 'UNFORMATTED',               &
          access     = 'STREAM',                    & ! SEQUENTIAL
          action     = 'WRITE',                     &
          convert    = trim(fd%type%format%endian)//"_ENDIAN",  &
          ! recordtype = 'STREAM',                    &
          ! buffered   = 'YES',                       &
          status     = 'REPLACE',                   &
          iostat     = ios)

      if(ios /= 0) print '("Problem creating file ",a,".")', trim(f)

      write(unit=fd%unit) "# vtk DataFile Version "//fd%type%version//newline//'VTK '//trim(fd%prefix)//newline//fd%format//newline
   end subroutine open_file_leg_bin

   subroutine open_file_XML_ascii( fd, proc_rank, num_procs, restart )
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout) :: fd
      integer, optional, intent(in)         :: proc_rank, num_procs, restart

      character(len=10)                     :: rank, snapshot
      character(len=80)                     :: f
      !character(len=80)                     :: char_mesh
      !character(len=256)                    :: MAIN_header
      integer                               :: ios
      !-------------------------------------------------------------------------

      if ( present(proc_rank) .and. present(num_procs) ) then

        iproc    = proc_rank
        nb_procs = num_procs

      else if ( present(proc_rank) ) then
         print *,"[ERROR]     ","VTK_open_file","Both PROC_RANK and NUM_PROCS arguments must be present."
         stop
      else if ( present(num_procs) ) then
         print *,"[ERROR]     ","VTK_open_file","Both PROC_RANK and NUM_PROCS arguments must be present."
         stop
      end if
      if ((fd%first) .and. (present(restart))) then
         fd%restart=restart
         fd%counter=restart
         fd%first=.false.
      end if
      fd%counter=fd%counter+1
      write(snapshot,'(i8)') fd%counter
      if ( present(proc_rank) ) then
        write(rank,'(i8)') iproc
        fd%ext='.p'//fd%ext(1:4)
        f=trim(fd%prefix)//"_"//trim(adjustl(rank))//"_"//trim(adjustl(snapshot))//fd%ext
      else
        f=trim(fd%prefix)//"_"//trim(adjustl(snapshot))//fd%ext
      end if

        open(newunit    = fd%unit,          &
             file       = trim(adjustl(f)), &
             form       = 'FORMATTED',      & ! FORMATTED
             access     = 'STREAM',         & ! SEQUENTIAL
             action     = 'WRITE',          &
             status     = 'REPLACE',        &
             iostat     = ios)

      if(ios /= 0) print '("Problem creating file ",a,".")', trim(f)

      write(unit=fd%unit, fmt='(100A)') '<?xml version="1.0"?>'
   end subroutine open_file_XML_ascii

   subroutine open_file_XML_bin( fd, proc_rank, num_procs, restart )
      !=========================================================================
      !
      !=========================================================================
      implicit none

      integer, optional, intent(in)         :: proc_rank, num_procs, restart
      class(VTKfileHandler), intent(inout) :: fd
      character(len=10)                     :: rank, snapshot
      character(len=80)                     :: f
      integer                               :: ios
      !-------------------------------------------------------------------------

      if ( present(proc_rank) .and. present(num_procs) ) then

        iproc    = proc_rank
        nb_procs = num_procs

      else if ( present(proc_rank) ) then
         print *,"[ERROR]     ","VTK_open_file","Both PROC_RANK and NUM_PROCS arguments must be present."
         stop
      else if ( present(num_procs) ) then
         print *,"[ERROR]     ","VTK_open_file","Both PROC_RANK and NUM_PROCS arguments must be present."
         stop
      end if
      if ((fd%first) .and. (present(restart))) then
         fd%restart=restart
         fd%counter=restart
         fd%first=.false.
      end if
      fd%counter=fd%counter+1
      write(snapshot,'(i8)') fd%counter
      if ( present(proc_rank) ) then
        write(rank,'(i8)') iproc
        fd%ext='.p'//fd%ext(1:4)
        f=trim(fd%prefix)//"_"//trim(adjustl(rank))//"_"//trim(adjustl(snapshot))//fd%ext
      else
        f=trim(fd%prefix)//"_"//trim(adjustl(snapshot))//fd%ext
      end if

      open(newunit    = fd%unit,                     &
          file       = trim(adjustl(f)),            &
          form       = 'UNFORMATTED',               &
          access     = 'STREAM',                    & ! SEQUENTIAL
          action     = 'WRITE',                     &
          ! convert    = trim(fd%type%format%endian)//"_ENDIAN",  &
          ! recordtype = 'STREAM',                    &
          ! buffered   = 'YES',                       &
          status     = 'REPLACE',                   &
          iostat     = ios)

      if(ios/=0) then
         print*, "[ERROR] Problem creating the file: ",trim(f),'endian: '//trim(fd%type%format%endian)//"_ENDIAN"
         ! ERROR STOP 66610   Not currently supported by PGI
         STOP 66610
      endif

      write(unit=fd%unit) '<?xml version="1.0"?>'//newline
   end subroutine open_file_XML_bin

   subroutine close_file_XML_bin(fd)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      logical :: file_opened, file_exist
      integer :: ios
      !-------------------------------------------------------------------------

      inquire(unit=fd%unit, exist=file_exist, opened=file_opened, iostat=ios)
      if ((file_opened) .and. (file_exist)) then

         write(unit=fd%unit) '    </PointData>'//newline
         write(unit=fd%unit) '   </Piece>'//newline
         write(unit=fd%unit) fd%type%format%geom%XMLend//newline
         write(unit=fd%unit) ' </VTKFile>'

         close(unit=fd%unit)
      else
         print *,"[WARNing]   ","VTK_close_file","No such file to close. Please, check file descriptor."
         print *,"[WARNing]   ","VTK_close_file", 'file_exist:', file_exist, 'file_opened:', file_opened, 'ios:', ios
      end if
   end subroutine close_file_XML_bin

   subroutine close_file_XML_ascii(fd)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      logical :: file_opened, file_exist
      integer :: ios
      !-------------------------------------------------------------------------

      inquire(unit=fd%unit, exist=file_exist, opened=file_opened, iostat=ios)
      if ((file_opened) .and. (file_exist)) then
         write(unit=fd%unit,fmt=*) '   </PointData>'
         write(unit=fd%unit,fmt=*) '  </Piece>'
         write(unit=fd%unit,fmt=*) fd%type%format%geom%XMLend
         write(unit=fd%unit,fmt=*) '</VTKFile>'
         close(unit=fd%unit)
      else
         print *,"[WARNing]   ","VTK_close_file","No such file to close. Please, check file descriptor."
         print *,"[WARNing]   ","VTK_close_file", 'file_exist:', file_exist, 'file_opened:', file_opened, 'ios:', ios
      end if
   end subroutine close_file_XML_ascii

   subroutine close_file_leg_bin(fd)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      logical :: file_opened, file_exist
      integer :: ios
      !-------------------------------------------------------------------------

      inquire(unit=fd%unit, exist=file_exist, opened=file_opened, iostat=ios)
      if ((file_opened) .and. (file_exist)) then
         ! Do nothing special for LEGACY, execpt close.
         close(unit=fd%unit)
      else
        print *,"[WARNing]   ","VTK_close_file","No such file to close. Please, check file descriptor."
        print *,"[WARNing]   ","VTK_close_file", 'file_exist:', file_exist, 'file_opened:', file_opened, 'ios:', ios
      end if
   end subroutine close_file_leg_bin

   subroutine close_file_leg_ascii(fd)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      logical :: file_opened, file_exist
      integer :: ios
      !-------------------------------------------------------------------------

      inquire(unit=fd%unit, exist=file_exist, opened=file_opened, iostat=ios)
      if ((file_opened) .and. (file_exist)) then
         ! Do nothing special for LEGACY
         close(unit=fd%unit)
      else
        print *,"[WARNing]   ","VTK_close_file","No such file to close. Please, check file descriptor."
        print *,"[WARNing]   ","VTK_close_file", 'file_exist:', file_exist, 'file_opened:', file_opened, 'ios:', ios
      end if
   end subroutine close_file_leg_ascii

   subroutine write_scalar3d_1r3_dr_leg_ascii(fd, name, s)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real (kind=dr), intent(in) :: s(:,:,:)
      character(len=256)  ::  VAR_header!, uname
      !-------------------------------------------------------------------------

      ! Check size compatiblity
      if ((size(s,dim=1) /= fd%ni) .or. &
         (size(s,dim=2) /= fd%nj) .or. &
         (size(s,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible V component and mesh sizes."

      ! write the scalar field converted in simple real float numbers
      VAR_header="SCALARS "//trim(adjustl(name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(s(1:fd%ni, 1:fd%nj, 1:fd%nk), kind=sr)
   end subroutine write_scalar3d_1r3_dr_leg_ascii

   subroutine write_scalar3d_1r3_dr_leg_bin(fd, name, s)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in) :: s(:,:,:)
      character(len=256)  ::  VAR_header
      !-------------------------------------------------------------------------

      ! Check size compatiblity
      if ((size(s,dim=1) /= fd%ni) .or. &
         (size(s,dim=2) /= fd%nj) .or. &
         (size(s,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible V component and mesh sizes."

      ! write the scalar field converted in simple real float numbers
      VAR_header="SCALARS "//trim(adjustl(name))//" float "//newline//"LOOKUP_TABLE default"//newline
      write(unit=fd%unit) trim(VAR_header), real(s(1:fd%ni, 1:fd%nj, 1:fd%nk), kind=sr)
   end subroutine write_scalar3d_1r3_dr_leg_bin

   subroutine write_scalar3d_1r3_dr_XML_ascii(fd, name, s)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:) :: s
      ! integer                                         :: i, j, k, code=0
      ! character(len=256)                              :: uname!, VAR_header
      !-------------------------------------------------------------------------

      if ((size(s,dim=1) /= fd%ni) .or. &
         (size(s,dim=2) /= fd%nj) .or. &
         (size(s,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      write(unit=fd%unit,fmt=*) '    <DataArray type="Float32" Name="'//&
                      &trim(adjustl(name))//'" NumberOfComponents="1" format="ascii">'
      write(unit=fd%unit,fmt=*) real(s(1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
      write(unit=fd%unit,fmt=*) '    </DataArray>'
   end subroutine write_scalar3d_1r3_dr_XML_ascii

   subroutine write_scalar3d_1r3_dr_XML_bin(fd, name, s)
      !=========================================================================
      !
      !=========================================================================
      use BeFor64
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:) :: s
      ! character(len=256)                              :: uname!, VAR_header
      integer                                         :: code=0
      integer                                         :: i,j,k,n
      real(kind=dr), allocatable, dimension(:)  :: velocity
      integer(kind=vsi), allocatable::  XYZp(:)    !< Packed data.
      character(len=:), allocatable::  XYZ64      !< X, Y, Z coordinates encoded in base64.
      !-------------------------------------------------------------------------

      !
      if ((size(s,dim=1) /= fd%ni) .or. &
         (size(s,dim=2) /= fd%nj) .or. &
         (size(s,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      if (.not.allocated(velocity)) then
         allocate(velocity(1*(fd%ni*fd%nj*fd%nk)) ,STAT=code)
         if ( code /= 0 ) then
            print *,"[ERROR]     ","write_scalar3d_1r3_dr_XML_bin","Not enough memory to allocate VELOCITY array"
            stop
         endif
      end if

      n= 0
      do k=1, fd%nk
         do j=1, fd%nj
            do i=1, fd%ni
               n= n+1
               velocity(1+(n-1)*1:1+(n-1)*1+0)=[s(i,j,k)]
            end do
         end do
      end do

      !
      write(unit=fd%unit) '     <DataArray type="Float32" Name="'//&
                      &trim(adjustl(name))//'" NumberOfComponents="1" format="binary">'//newline
      call pack_data(a1=[int((1*(fd%ni*fd%nj*fd%nk)+1)*sr,di)], a2=real(Velocity, sr) ,packed=XYZp)
      call b64_encode(n=XYZp,code=XYZ64)
      write(unit=fd%unit) '      ', XYZ64, newline
      write(unit=fd%unit) '     </DataArray>'//newline
   end subroutine write_scalar3d_1r3_dr_XML_bin

   subroutine write_array3d_1r4_dr_leg_ascii(fd, name, v)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in) :: v(:,:,:,:)
      !-------------------------------------------------------------------------

      ! Close the file with the correct VTK type and format.
      print*, '[ERROR]',' VTK subroutine write_array3d_1r4_dr_leg_ascii not yet implemented'
   end subroutine write_array3d_1r4_dr_leg_ascii

   subroutine write_array3d_1r4_dr_leg_bin(fd, name, v)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in) :: v(:,:,:,:)
      !-------------------------------------------------------------------------

      ! Close the file with the correct VTK type and format.
      print*, '[ERROR]',' VTK subroutine write_array3d_1r4_dr_leg_bin not yet implemented'
   end subroutine write_array3d_1r4_dr_leg_bin

   subroutine write_array3d_1r4_dr_XML_ascii(fd, name, v)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in) ::v(:,:,:,:)
      !-------------------------------------------------------------------------

      ! Close the file with the correct VTK type and format.
      print*, '[ERROR]',' VTK subroutine write_array3d_1r4_dr_XML_ascii not yet implemented'
   end subroutine write_array3d_1r4_dr_XML_ascii

   subroutine write_array3d_1r4_dr_XML_bin(fd, name, v)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in) :: v(:,:,:,:)
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_array3d_1r4_dr_XML_bin not yet implemented'
   end subroutine write_array3d_1r4_dr_XML_bin

   subroutine write_array3d_3r3_dr_leg_ascii(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:) :: vx, vy, vz
      real(kind=dr), allocatable, dimension(:,:,:,:)  :: velocity
      integer                                         :: code=0
      character(len=256)                              ::  VAR_header!, uname
      !-------------------------------------------------------------------------

      if ((size(vx,dim=1) /= fd%ni) .or. &
         (size(vx,dim=2) /= fd%nj) .or. &
         (size(vx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      if ((size(vy,dim=1) /= fd%ni) .or. &
         (size(vy,dim=2) /= fd%nj) .or. &
         (size(vy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      if ((size(vz,dim=1) /= fd%ni) .or. &
         (size(vz,dim=2) /= fd%nj) .or. &
         (size(vz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."

      if (.not.allocated(velocity)) then
         allocate(velocity(3,fd%ni,fd%nj,fd%nk),STAT=code)
         if ( code /= 0 ) then
            print *,"[ERROR]     ","VTK_write_var","Not enough memory to allocate VELOCITY array"
            stop
         endif
      end if

      ! do k=1, fd%nk
      !    do j=1, fd%nj
      !       do i=1, fd%ni
               velocity(1,:,:,:) = vx(:,:,:)
               velocity(2,:,:,:) = vy(:,:,:)
               velocity(3,:,:,:) = vz(:,:,:)
      !       end do
      !    end do
      ! end do

      write(unit=fd%unit, fmt=*) "VECTORS "//trim(adjustl(name))//" float "
      write(unit=fd%unit, fmt=*) real(velocity(:,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="X_"//name
      VAR_header="SCALARS "//trim(adjustl("X_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(velocity(1,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Y_"//name
      VAR_header="SCALARS "//trim(adjustl("Y_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(velocity(2,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Z_"//name
      VAR_header="SCALARS "//trim(adjustl("Z_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) VAR_header
      write(unit=fd%unit, fmt=*) real(velocity(3,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
   end subroutine write_array3d_3r3_dr_leg_ascii

   subroutine write_array3d_3r3_dr_leg_bin(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:) :: vx, vy, vz
      real(kind=dr), allocatable, dimension(:,:,:,:)  :: velocity
      integer                                         :: code=0
      character(len=256)                              ::  VAR_header!, uname
      !-------------------------------------------------------------------------

      if ((size(vx,dim=1) /= fd%ni) .or. &
         (size(vx,dim=2) /= fd%nj) .or. &
         (size(vx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      if ((size(vy,dim=1) /= fd%ni) .or. &
         (size(vy,dim=2) /= fd%nj) .or. &
         (size(vy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      if ((size(vz,dim=1) /= fd%ni) .or. &
         (size(vz,dim=2) /= fd%nj) .or. &
         (size(vz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."

      if (.not.allocated(velocity)) then
         allocate(velocity(3,fd%ni,fd%nj,fd%nk),STAT=code)
         if ( code /= 0 ) then
            print *,"[ERROR]     ","VTK_write_var","Not enough memory to allocate VELOCITY array"
            stop
         endif
      end if

      ! do k=1, fd%nk
      !    do j=1, fd%nj
      !       do i=1, fd%ni
               velocity(1,:,:,:) = vx(:,:,:)
               velocity(2,:,:,:) = vy(:,:,:)
               velocity(3,:,:,:) = vz(:,:,:)
      !       end do
      !    end do
      ! end do

      VAR_header="VECTORS "//trim(adjustl(name))//" float "//newline
      write(unit=fd%unit) trim(VAR_header),real(velocity(:,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="X_"//name
      ! VAR_header="SCALARS "//trim(adjustl(uname))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header),real(velocity(1,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Y_"//name
      ! VAR_header="SCALARS "//trim(adjustl(uname))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header),real(velocity(2,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Z_"//name
      ! VAR_header="SCALARS "//trim(adjustl(uname))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header),real(velocity(3,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
   end subroutine write_array3d_3r3_dr_leg_bin

   subroutine write_array3d_3r3_dr_XML_ascii(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)                :: name
      real (kind=dr), intent(in), dimension(:,:,:) :: vx, vy, vz
      real(kind=dr), allocatable, dimension(:,:,:,:)  :: velocity
      integer                                         :: i, j, k, code=0
      character(len=256)                              :: uname!, VAR_header
      !-------------------------------------------------------------------------

      if ((size(vx,dim=1) /= fd%ni) .or. &
         (size(vx,dim=2) /= fd%nj) .or. &
         (size(vx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      if ((size(vy,dim=1) /= fd%ni) .or. &
         (size(vy,dim=2) /= fd%nj) .or. &
         (size(vy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      if ((size(vz,dim=1) /= fd%ni) .or. &
         (size(vz,dim=2) /= fd%nj) .or. &
         (size(vz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."

      if (.not.allocated(velocity)) then
         allocate(velocity(3,fd%ni,fd%nj,fd%nk),STAT=code)
         if ( code /= 0 ) then
            print *,"[ERROR]     ","VTK_write_var","Not enough memory to allocate VELOCITY array"
            stop
         endif
      end if

      ! do k=1, fd%nk
      !    do j=1, fd%nj
      !       do i=1, fd%ni
               velocity(1,:,:,:) = vx(:,:,:)
               velocity(2,:,:,:) = vy(:,:,:)
               velocity(3,:,:,:) = vz(:,:,:)
      !       end do
      !    end do
      ! end do

      write(unit=fd%unit,fmt=*) '    <DataArray type="Float32" Name="'//&
                      &trim(adjustl(name))//'" NumberOfComponents="3" format="ascii">'
      write(unit=fd%unit,fmt=*) real(Velocity(:,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
      write(unit=fd%unit,fmt=*) '    </DataArray>'

      uname="X_"//name
      write(unit=fd%unit,fmt=*) '    <DataArray type="Float32" Name="'//&
                      &trim(adjustl(uname))//'" NumberOfComponents="1" format="ascii">'
      write(unit=fd%unit,fmt=*) real(Velocity(1,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
      write(unit=fd%unit,fmt=*) '    </DataArray>'

      uname="Y_"//name
      write(unit=fd%unit,fmt=*) '    <DataArray type="Float32" Name="'//&
                      &trim(adjustl(uname))//'" NumberOfComponents="1" format="ascii">'
      write(unit=fd%unit,fmt=*) real(Velocity(2,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
      write(unit=fd%unit,fmt=*) '    </DataArray>'

      uname="Z_"//name
      write(unit=fd%unit,fmt=*) '    <DataArray type="Float32" Name="'//&
                      &trim(adjustl(uname))//'" NumberOfComponents="1" format="ascii">'
      write(unit=fd%unit,fmt=*) real(Velocity(3,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
      write(unit=fd%unit,fmt=*) '    </DataArray>'

      if (allocated(velocity)) deallocate(velocity)
   end subroutine write_array3d_3r3_dr_XML_ascii

   subroutine write_array3d_3r3_dr_XML_bin(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      use BeFor64
      implicit none

      class(VTKfileHandler), intent(in)              :: fd
      character(len=*), intent(in)                    :: name
      real(kind=dr), intent(in), dimension(:,:,:)     :: vx, vy, vz
      real(kind=dr), allocatable, dimension(:)  :: velocity
      integer                                         :: code=0
      integer                                         :: i,j,k,n
      integer(kind=vsi), allocatable::  XYZp(:)    !< Packed data.
      character(len=:), allocatable::  XYZ64      !< X, Y, Z coordinates encoded in base64.
      !-------------------------------------------------------------------------


      if ((size(vx,dim=1) /= fd%ni) .or. &
         (size(vx,dim=2) /= fd%nj) .or. &
         (size(vx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      if ((size(vy,dim=1) /= fd%ni) .or. &
         (size(vy,dim=2) /= fd%nj) .or. &
         (size(vy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      if ((size(vz,dim=1) /= fd%ni) .or. &
         (size(vz,dim=2) /= fd%nj) .or. &
         (size(vz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."

      if (.not.allocated(velocity)) then
         allocate(velocity(3*(fd%ni*fd%nj*fd%nk)) ,STAT=code)
         if ( code /= 0 ) then
            print *,"[ERROR]     ","VTK_write_var","Not enough memory to allocate VELOCITY array"
            stop
         endif
      end if

      n= 0
      do k=1, fd%nk
         do j=1, fd%nj
            do i=1, fd%ni
               n= n+1
               velocity(1+(n-1)*3:1+(n-1)*3+2)=[vX(i,j,k),vY(i,j,k),vZ(i,j,k)]
            end do
         end do
      end do

      write(unit=fd%unit) '     <DataArray type="Float32" Name="'//&
                      &trim(adjustl(name))//'" NumberOfComponents="3" format="binary">'//newline
      call pack_data(a1=[int((3*(fd%ni*fd%nj*fd%nk)+1)*sr,di)], a2=real(Velocity, sr) ,packed=XYZp)
      call b64_encode(n=XYZp,code=XYZ64)
      write(unit=fd%unit) '      ', XYZ64, newline
      write(unit=fd%unit) '     </DataArray>'//newline

      if (allocated(velocity)) deallocate(velocity)
   end subroutine write_array3d_3r3_dr_XML_bin

   subroutine write_matrix3d_1r5_dr_leg_ascii(fd, name, m)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real (kind=dr), intent(in) :: m(:,:,:,:,:)
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_1r5_dr_leg_ascii not yet implemented'
   end subroutine write_matrix3d_1r5_dr_leg_ascii

   subroutine write_matrix3d_1r5_dr_leg_bin(fd, name, m)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real (kind=dr), intent(in) :: m(:,:,:,:,:)
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_1r5_dr_leg_bin not yet implemented'
   end subroutine write_matrix3d_1r5_dr_leg_bin

   subroutine write_matrix3d_1r5_dr_XML_ascii(fd, name, m)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real (kind=dr), intent(in) :: m(:,:,:,:,:)
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_1r5_dr_XML_ascii not yet implemented'
   end subroutine write_matrix3d_1r5_dr_XML_ascii

   subroutine write_matrix3d_1r5_dr_XML_bin(fd, name, m)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real (kind=dr), intent(in) :: m(:,:,:,:,:)
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_1r5_dr_XML_bin not yet implemented'
   end subroutine write_matrix3d_1r5_dr_XML_bin

   subroutine write_matrix3d_9r3_dr_leg_ascii(fd, name, mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real(kind=dr), intent(in), dimension(:,:,:)  :: mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz
      real(kind=dr), dimension(:,:,:,:), allocatable  :: matrix3D
      integer  :: code=0
      character(len=256)  ::  VAR_header
      !-------------------------------------------------------------------------

      if ((size(mxx,dim=1) /= fd%ni) .or. &
         (size(mxx,dim=2) /= fd%nj) .or. &
         (size(mxx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      if ((size(mxy,dim=1) /= fd%ni) .or. &
         (size(mxy,dim=2) /= fd%nj) .or. &
         (size(mxy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      if ((size(mxz,dim=1) /= fd%ni) .or. &
         (size(mxz,dim=2) /= fd%nj) .or. &
         (size(mxz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."

      if ((size(myx,dim=1) /= fd%ni) .or. &
         (size(myx,dim=2) /= fd%nj) .or. &
         (size(myx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      if ((size(myy,dim=1) /= fd%ni) .or. &
         (size(myy,dim=2) /= fd%nj) .or. &
         (size(myy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      if ((size(myz,dim=1) /= fd%ni) .or. &
         (size(myz,dim=2) /= fd%nj) .or. &
         (size(myz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."

      if ((size(mzx,dim=1) /= fd%ni) .or. &
         (size(mzx,dim=2) /= fd%nj) .or. &
         (size(mzx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      if ((size(mzy,dim=1) /= fd%ni) .or. &
         (size(mzy,dim=2) /= fd%nj) .or. &
         (size(mzy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      if ((size(mzz,dim=1) /= fd%ni) .or. &
         (size(mzz,dim=2) /= fd%nj) .or. &
         (size(mzz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."


      if (.not.allocated(matrix3D)) then
         allocate(matrix3D(9,fd%ni,fd%nj,fd%nk),STAT=code)
         if ( code /= 0 ) then
            print *,"[ERROR]     ","VTK_write_var","Not enough memory to allocate matrix3D array"
            stop
         endif
      end if

      ! do k=1, fd%nk
      !    do j=1, fd%nj
      !       do i=1, fd%ni
               matrix3D(1,:,:,:) = mxx(:,:,:)
               matrix3D(2,:,:,:) = mxy(:,:,:)
               matrix3D(3,:,:,:) = mxz(:,:,:)

               matrix3D(4,:,:,:) = myx(:,:,:)
               matrix3D(5,:,:,:) = myy(:,:,:)
               matrix3D(6,:,:,:) = myz(:,:,:)

               matrix3D(7,:,:,:) = mzx(:,:,:)
               matrix3D(8,:,:,:) = mzy(:,:,:)
               matrix3D(9,:,:,:) = mzz(:,:,:)

      !       end do
      !    end do
      ! end do

      write(unit=fd%unit, fmt=*) "TENSORS "//trim(adjustl(name))//" float "
      write(unit=fd%unit, fmt=*) real(matrix3D(:,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="X_"//name
      VAR_header="SCALARS "//trim(adjustl("XX_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(1,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Y_"//name
      VAR_header="SCALARS "//trim(adjustl("XY_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(2,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Z_"//name
      VAR_header="SCALARS "//trim(adjustl("XZ_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(3,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="X_"//name
      VAR_header="SCALARS "//trim(adjustl("YX_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(4,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Y_"//name
      VAR_header="SCALARS "//trim(adjustl("YY_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(5,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Z_"//name
      VAR_header="SCALARS "//trim(adjustl("YZ_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(6,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="X_"//name
      VAR_header="SCALARS "//trim(adjustl("ZX_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(7,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Y_"//name
      VAR_header="SCALARS "//trim(adjustl("ZY_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(8,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! uname="Z_"//name
      VAR_header="SCALARS "//trim(adjustl("ZZ_"//name))//" float "//newline//"LOOKUP_TABLE default"
      write(unit=fd%unit, fmt=*) trim(VAR_header)
      write(unit=fd%unit, fmt=*) real(matrix3D(9,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
   end subroutine write_matrix3d_9r3_dr_leg_ascii

   subroutine write_matrix3d_9r3_dr_leg_bin(fd, name, mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real(kind=dr), intent(in), dimension(:,:,:)  :: mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz
      real(kind=dr), dimension(:,:,:,:), allocatable  :: matrix3D
      integer  :: code=0
      character(len=256)  ::  VAR_header
      !-------------------------------------------------------------------------

      ! if ((size(mxx,dim=1) /= fd%ni) .or. &
      !    (size(mxx,dim=2) /= fd%nj) .or. &
      !    (size(mxx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      ! if ((size(mxy,dim=1) /= fd%ni) .or. &
      !    (size(mxy,dim=2) /= fd%nj) .or. &
      !    (size(mxy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      ! if ((size(mxz,dim=1) /= fd%ni) .or. &
      !    (size(mxz,dim=2) /= fd%nj) .or. &
      !    (size(mxz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."

      ! if ((size(myx,dim=1) /= fd%ni) .or. &
      !    (size(myx,dim=2) /= fd%nj) .or. &
      !    (size(myx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      ! if ((size(myy,dim=1) /= fd%ni) .or. &
      !    (size(myy,dim=2) /= fd%nj) .or. &
      !    (size(myy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      ! if ((size(myz,dim=1) /= fd%ni) .or. &
      !    (size(myz,dim=2) /= fd%nj) .or. &
      !    (size(myz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."

      ! if ((size(mzx,dim=1) /= fd%ni) .or. &
      !    (size(mzx,dim=2) /= fd%nj) .or. &
      !    (size(mzx,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible X component and mesh sizes."

      ! if ((size(mzy,dim=1) /= fd%ni) .or. &
      !    (size(mzy,dim=2) /= fd%nj) .or. &
      !    (size(mzy,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Y component and mesh sizes."

      ! if ((size(mzz,dim=1) /= fd%ni) .or. &
      !    (size(mzz,dim=2) /= fd%nj) .or. &
      !    (size(mzz,dim=3) /= fd%nk)) print *,"[WARNing]   ","VTK_write_var","Incompatible Z component and mesh sizes."


      if (.not.allocated(matrix3D)) then
         allocate(matrix3D(9,fd%ni,fd%nj,fd%nk),STAT=code)
         if ( code /= 0 ) then
            print *,"[ERROR]     ","VTK_write_var","Not enough memory to allocate matrix3D array"
            stop
         endif
      end if

      ! do k=1, fd%nk
      !    do j=1, fd%nj
      !       do i=1, fd%ni
               matrix3D(1,:,:,:) = mxx(:,:,:)
               matrix3D(2,:,:,:) = mxy(:,:,:)
               matrix3D(3,:,:,:) = mxz(:,:,:)

               matrix3D(4,:,:,:) = myx(:,:,:)
               matrix3D(5,:,:,:) = myy(:,:,:)
               matrix3D(6,:,:,:) = myz(:,:,:)

               matrix3D(7,:,:,:) = mzx(:,:,:)
               matrix3D(8,:,:,:) = mzy(:,:,:)
               matrix3D(9,:,:,:) = mzz(:,:,:)

      !       end do
      !    end do
      ! end do

      VAR_header="TENSORS "//trim(adjustl(name))//" float "//newline
      write(unit=fd%unit) trim(VAR_header),real(matrix3D(:,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="XX_"//name
      ! VAR_header="SCALARS "//trim(adjustl("XX_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(1,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="XY_"//name
      ! VAR_header="SCALARS "//trim(adjustl("XY_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(2,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="XZ_"//name
      ! VAR_header="SCALARS "//trim(adjustl("XZ_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(3,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="YX_"//name
      ! VAR_header="SCALARS "//trim(adjustl("YX_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(4,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="YY_"//name
      ! VAR_header="SCALARS "//trim(adjustl("YY_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(5,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="X_"//name
      ! VAR_header="SCALARS "//trim(adjustl("YZ_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(6,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="X_"//name
      ! VAR_header="SCALARS "//trim(adjustl("ZX_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(7,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="X_"//name
      ! VAR_header="SCALARS "//trim(adjustl("ZY_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(8,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)

      ! ! uname="X_"//name
      ! VAR_header="SCALARS "//trim(adjustl("ZZ_"//name))//" float "//newline//"LOOKUP_TABLE default"//newline
      ! write(unit=fd%unit) trim(VAR_header), real(matrix3D(9,1:fd%ni,1:fd%nj,1:fd%nk),kind=sr)
   end subroutine write_matrix3d_9r3_dr_leg_bin

   subroutine write_matrix3d_9r3_dr_XML_ascii(fd, name, mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real(kind=dr), intent(in), dimension(:,:,:)  :: mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_9r3_dr_XML_ascii not yet implemented'
   end subroutine write_matrix3d_9r3_dr_XML_ascii

   subroutine write_matrix3d_9r3_dr_XML_bin(fd, name, mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real(kind=dr), intent(in), dimension(:,:,:)  :: mxx, mxy, mxz, myx, myy, myz, mzx, mzy, mzz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_9r3_dr_XML_bin not yet implemented'
   end subroutine write_matrix3d_9r3_dr_XML_bin

   subroutine write_matrix3d_3r4_dr_leg_ascii(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real(kind=dr), intent(in), dimension(:,:,:,:)  :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_3r4_dr_leg_ascii not yet implemented'
   end subroutine write_matrix3d_3r4_dr_leg_ascii

   subroutine write_matrix3d_3r4_dr_leg_bin(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real(kind=dr), intent(in), dimension(:,:,:,:)  :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_3r4_dr_leg_bin not yet implemented'
   end subroutine write_matrix3d_3r4_dr_leg_bin

   subroutine write_matrix3d_3r4_dr_XML_ascii(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real(kind=dr), intent(in), dimension(:,:,:,:)  :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_3r4_dr_XML_ascii not yet implemented'
   end subroutine write_matrix3d_3r4_dr_XML_ascii

   subroutine write_matrix3d_3r4_dr_XML_bin(fd, name, vx, vy, vz)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(in) :: fd
      character(len=*), intent(in)  :: name
      real(kind=dr), intent(in), dimension(:,:,:,:)  :: vx, vy, vz
      !-------------------------------------------------------------------------

      !
      print*, '[ERROR]',' VTK subroutine write_matrix3d_3r4_dr_XML_bin not yet implemented'
   end subroutine write_matrix3d_3r4_dr_XML_bin


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!    GEOMETRY Functions: Mesh    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! VTKfileHandler
   subroutine write_mesh_3d_npspor(fd, np, space, origin)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      integer(kind=di), intent(in) :: np(1:3)
      real(kind=dr), intent(in)  :: space(1:3)
      real(kind=dr), intent(in)  :: origin(1:3)
      !-------------------------------------------------------------------------

      !
      call fd%type%format%geom%write_mesh_3d_npspor(fd, np, space, origin)
   end subroutine write_mesh_3d_npspor

! IMAGE
   subroutine write_mesh_3d_leg_ascii_ima_npspor(fd, np, space, origin)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      integer(kind=di), intent(in) :: np(1:3)
      real(kind=dr), intent(in)  :: space(1:3)
      real(kind=dr), intent(in)  :: origin(1:3)
      character(len=90)                         :: buf1
      !-------------------------------------------------------------------------

      fd%ni=np(1) ; fd%nj=np(2) ; fd%nk=np(3)

      write(buf1,'(i8," ",i8," ",i8)')  fd%ni, fd%nj, fd%nk
      write(unit=fd%unit, fmt='(100A)') "DATASET STRUCTURED_POINTS"
      write(unit=fd%unit, fmt='(100A)') "DIMENSIONS "//trim(adjustl(buf1))
      ! write(unit=fd%unit, fmt=*) "ASPECT_RATIO 1 1 1"  ! Depreciate since version 2.0
      write(buf1,'(f20.10," ",f20.10," ",f20.10)') space(1), space(2), space(3)
      write(unit=fd%unit, fmt='(100A)') "SPACING "//trim(buf1)
      write(buf1,'(f20.10," ",f20.10," ",f20.10)')  origin(1), origin(2), origin(3)
      write(unit=fd%unit, fmt='(100A)') "ORIGIN "//trim(buf1)
      write(buf1,'(i8)') fd%ni*fd%nj*fd%nk
      write(unit=fd%unit, fmt='(100A)') "POINT_DATA "//trim(adjustl(buf1))
   end subroutine write_mesh_3d_leg_ascii_ima_npspor

   subroutine write_mesh_3d_leg_bin_ima_npspor(fd, np, space, origin)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      integer(kind=di), intent(in) :: np(1:3)
      real(kind=dr), intent(in)  :: space(1:3)
      real(kind=dr), intent(in)  :: origin(1:3)
      character(len=90)                         :: buf1
      !-------------------------------------------------------------------------

      !
      fd%ni=np(1) ; fd%nj=np(2) ; fd%nk=np(3)

      !
      write(unit=fd%unit) "DATASET STRUCTURED_POINTS"//newline
      write(buf1,'(i8," ",i8," ",i8)')  fd%ni, fd%nj, fd%nk
      write(unit=fd%unit) "DIMENSIONS "//trim(adjustl(buf1))//newline
      ! write(unit=fd%unit, fmt=*) "ASPECT_RATIO 1 1 1"  ! Depreciate since version 2.0
      write(buf1,'(f20.10," ",f20.10," ",f20.10)') space(1), space(2), space(3)
      write(unit=fd%unit) "SPACING "//trim(buf1)//newline
      write(buf1,'(f20.10," ",f20.10," ",f20.10)')  origin(1), origin(2), origin(3)
      write(unit=fd%unit) "ORIGIN "//trim(buf1)//newline
      write(buf1,'(i8)') fd%ni*fd%nj*fd%nk
      write(unit=fd%unit) "POINT_DATA "//trim(adjustl(buf1))//newline
   end subroutine write_mesh_3d_leg_bin_ima_npspor

   subroutine write_mesh_3d_XML_ascii_ima_npspor(fd, np, space, origin)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      integer(kind=di), intent(in) :: np(1:3)
      real(kind=dr), intent(in)  :: space(1:3)
      real(kind=dr), intent(in)  :: origin(1:3)
      character(len=30)                         :: buf1, buf2, buf3
      character(len=90)                         :: buf4, buf5
      ! character(len=:), allocatable            :: buffvar
      !-------------------------------------------------------------------------

      !
      fd%ni=np(1) ; fd%nj=np(2) ; fd%nk=np(3)

      !
      write(buf1,'(I6)') fd%ni
      write(buf2,'(I6)') fd%nj
      write(buf3,'(I6)') fd%nk

      write(buf4,'(f20.10," ",f20.10," ",f20.10)') origin(1), origin(2), origin(3)
      write(buf5,'(f20.10," ",f20.10," ",f20.10)') space(1), space(2), space(3)

      !
      write(unit=fd%unit, fmt=*) '<VTKFile type="'//'ImageData'//&
                  &'" version="'//trim(fd%type%version)//'" format="'//trim(fd%format)//'">'
      write(unit=fd%unit, fmt=*) ' <ImageData WholeExtent="1 '//trim(adjustl(buf1))//&
                             &' 1 '//trim(adjustl(buf2))//' 1 '//trim(adjustl(buf3))//&
                             &'" Origin="'//trim(adjustl(buf4))//'" Spacing="'//trim(adjustl(buf5))&
                             &//'">'
      fd%type%format%geom%XMLend=' </ImageData>'
      write(unit=fd%unit, fmt=*) '  <Piece Extent="1 '//trim(adjustl(buf1))//' 1 '//&
                             &trim(adjustl(buf2))//' 1 '//trim(adjustl(buf3))//'">'
      write(unit=fd%unit, fmt=*) '   <PointData>'
   end subroutine write_mesh_3d_XML_ascii_ima_npspor

   subroutine write_mesh_3d_XML_bin_ima_npspor(fd, np, space, origin)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      integer(kind=di), intent(in) :: np(1:3)
      real(kind=dr), intent(in)  :: space(1:3)
      real(kind=dr), intent(in)  :: origin(1:3)
      character(len=30)                         :: buf1, buf2, buf3
      character(len=90)                         :: buf4, buf5
      ! character(len=:), allocatable            :: buffvar
      !-------------------------------------------------------------------------

      !
      fd%ni=np(1) ; fd%nj=np(2) ; fd%nk=np(3)

      !
      write(buf1,'(I6)') fd%ni
      write(buf2,'(I6)') fd%nj
      write(buf3,'(I6)') fd%nk

      write(buf4,'(f20.10," ",f20.10," ",f20.10)') origin(1), origin(2), origin(3)
      write(buf5,'(f20.10," ",f20.10," ",f20.10)') space(1), space(2), space(3)

      !
      write(unit=fd%unit) '<VTKFile type="'//'ImageData'//&
                  &'" version="'//trim(fd%type%version)//'" format="'//trim(fd%format)//&
                  &'" byte_order="'//trim(fd%type%format%endian)//'Endian">'//newline
      write(unit=fd%unit) ' <ImageData WholeExtent="1 '//trim(adjustl(buf1))//&
                             &' 1 '//trim(adjustl(buf2))//' 1 '//trim(adjustl(buf3))//&
                             &'" Origin="'//trim(adjustl(buf4))//'" Spacing="'//trim(adjustl(buf5))&
                             &//'">'//newline
      fd%type%format%geom%XMLend=' </ImageData>'
      write(unit=fd%unit) '  <Piece Extent="1 '//trim(adjustl(buf1))//' 1 '//&
                             &trim(adjustl(buf2))//' 1 '//trim(adjustl(buf3))//'">'//newline
      write(unit=fd%unit) '   <PointData>'//newline
   end subroutine write_mesh_3d_XML_bin_ima_npspor

   subroutine write_mesh_3d_dumb_npspor(fd, np, space, origin)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      integer(kind=di), intent(in) :: np(1:3)
      real(kind=dr), intent(in)  :: space(1:3)
      real(kind=dr), intent(in)  :: origin(1:3)
      !-------------------------------------------------------------------------

      !
      print*, '[WARNing] The write of a mesh/geomety using NumberPoint, space, origin &
            & is NOT compatible with ', fd%type_string//', ', fd%format//', ', fd%mesh_topology
   end subroutine write_mesh_3d_dumb_npspor

! RECTILINEAR
   subroutine write_mesh_3d_xyz(fd, x, y, z)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      real(kind=dr), intent(in), dimension(:)   :: x, y, z
      !-------------------------------------------------------------------------

      !
      call fd%type%format%geom%write_mesh_3d_xyz(fd, x, y, z)
   end subroutine write_mesh_3d_xyz

   subroutine write_mesh_3d_dumb_xyz(fd, x, y, z)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      real(kind=dr), intent(in), dimension(:)   :: x, y, z
      !-------------------------------------------------------------------------

      !
      print*, '[WARNing] The write of a mesh/geomety using x, y, z &
            & is NOT compatible with ', fd%type_string//', ', fd%format//', ', fd%mesh_topology
   end subroutine write_mesh_3d_dumb_xyz

   subroutine write_mesh_3d_leg_ascii_rect_xyz(fd, x, y, z)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      real(kind=dr), intent(in), dimension(:)   :: x, y, z
      character(len=30)                         :: buf1
      !-------------------------------------------------------------------------

      fd%ni=size(x) ; fd%nj=size(y) ; fd%nk=size(z)

      write(buf1,'(i8," ",i8," ",i8)')  size(x), size(y), size(z)
      write(unit=fd%unit, fmt='(100A)') "DATASET RECTILINEAR_GRID"
      write(unit=fd%unit, fmt='(100A)') "DIMENSIONS "//trim(adjustl(buf1))
      write(buf1, fmt='(i8)') fd%ni
      write(unit=fd%unit, fmt='(100A)') "X_COORDINATES "//trim(adjustl(buf1))//" float"
      write(unit=fd%unit, fmt=*)        real(x,kind=sr)
      write(buf1,'(i8)') fd%nj
      write(unit=fd%unit, fmt='(100A)') "Y_COORDINATES "//trim(adjustl(buf1))//" float"
      write(unit=fd%unit, fmt=*)  real(y,kind=sr)
      write(buf1,'(i8)') fd%nk
      write(unit=fd%unit, fmt='(100A)') "Z_COORDINATES "//trim(adjustl(buf1))//" float"
      write(unit=fd%unit, fmt=*)        real(z,kind=sr)
      write(buf1,'(i8)') fd%ni*fd%nj*fd%nk
      write(unit=fd%unit, fmt='(100A)') "POINT_DATA "//trim(adjustl(buf1))
   end subroutine write_mesh_3d_leg_ascii_rect_xyz

   subroutine write_mesh_3d_leg_bin_rect_xyz(fd, x, y, z)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      real(kind=dr), intent(in), dimension(:)   :: x, y, z
      character(len=30)                         :: buf1
      character(len=256)                        :: GRID_header
      !-------------------------------------------------------------------------

      fd%ni=size(x) ; fd%nj=size(y) ; fd%nk=size(z)

      write(buf1,'(i8," ",i8," ",i8)') size(x), size(y), size(z)
       GRID_header="DATASET RECTILINEAR_GRID"//newline//"DIMENSIONS "//trim(adjustl(buf1))//newline
      write(unit=fd%unit) trim(GRID_header)
       write(buf1, fmt='(i8)') fd%ni
       GRID_header="X_COORDINATES "//trim(adjustl(buf1))//" float"//newline
      write(unit=fd%unit) trim(GRID_header),real(x, kind=sr), newline
      write(buf1,'(i8)') fd%nj
       GRID_header="Y_COORDINATES "//trim(adjustl(buf1))//" float"//newline
      write(unit=fd%unit) trim(GRID_header),real(y, kind=sr), newline
      write(buf1,'(i8)') fd%nk
       GRID_header="Z_COORDINATES "//trim(adjustl(buf1))//" float"//newline
      write(unit=fd%unit) trim(GRID_header),real(0.0, kind=sr), newline
      write(buf1,'(i8)') fd%ni*fd%nj*fd%nk
       GRID_header="POINT_DATA "//trim(adjustl(buf1))//newline
      write(unit=fd%unit) trim(GRID_header)
   end subroutine write_mesh_3d_leg_bin_rect_xyz

   subroutine write_mesh_3d_XML_ascii_rect_xyz(fd, x, y, z)
      !=========================================================================
      !
      !=========================================================================
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      real(kind=dr), intent(in), dimension(:)   :: x, y, z
      character(len=30)                         :: buf1, buf2, buf3
      ! character(len=:), allocatable            :: buffvar
      !-------------------------------------------------------------------------

      fd%ni=size(x) ; fd%nj=size(y) ; fd%nk=size(z)

      write(buf1,'(I6)') fd%ni
      write(buf2,'(I6)') fd%nj
      write(buf3,'(I6)') fd%nk

      write(unit=fd%unit, fmt=*) '<VTKFile type="'//'RectilinearGrid'//&
                  &'" version="0.1" format="'//trim(fd%format)//'" >'
      write(unit=fd%unit, fmt=*) ' <RectilinearGrid WholeExtent="1 '//trim(adjustl(buf1))//&
                             &' 1 '//trim(adjustl(buf2))//' 1 '//trim(adjustl(buf3))//'">'
      fd%type%format%geom%XMLend=' </RectilinearGrid>'
      write(unit=fd%unit, fmt=*) '  <Piece Extent="1 '//trim(adjustl(buf1))//' 1 '//&
                             &trim(adjustl(buf2))//' 1 '//trim(adjustl(buf3))//'">'
      write(unit=fd%unit, fmt=*) '   <Coordinates>'
      write(unit=fd%unit, fmt=*) '    <DataArray type="Float32" Name="X_COORDINATES" NumberOfComponents="1" &
                                 &format="ascii">'
      write(unit=fd%unit, fmt=*)        real(x(1:fd%ni),kind=sr)
      write(unit=fd%unit, fmt=*) '    </DataArray>'
      write(unit=fd%unit, fmt=*) '    <DataArray type="Float32" Name="Y_COORDINATES" NumberOfComponents="1" &
                                 &format="ascii">'
      write(unit=fd%unit, fmt=*)        real(y(1:fd%nj),kind=sr)
      write(unit=fd%unit, fmt=*) '    </DataArray>'
      write(unit=fd%unit, fmt=*) '    <DataArray type="Float32" Name="Z_COORDINATES" NumberOfComponents="1" &
                                 &format="ascii">'
      write(unit=fd%unit, fmt=*)        real(z,kind=sr)
      write(unit=fd%unit, fmt=*) '    </DataArray>'
      write(unit=fd%unit, fmt=*) '   </Coordinates>'
      write(unit=fd%unit, fmt=*) '   <PointData>'
   end subroutine write_mesh_3d_XML_ascii_rect_xyz

   subroutine write_mesh_3d_XML_bin_rect_xyz(fd, x, y, z)
      !=========================================================================
      !
      !=========================================================================
      use BeFor64
      implicit none

      class(VTKfileHandler), intent(inout)     :: fd
      real(kind=dr), intent(in), dimension(:)   :: x, y, z
      character(len=30)         :: buf1, buf2, buf3

      integer(kind=vsi), allocatable::   XYZp(:)    !< Packed data.
      character(len=:), allocatable::    XYZ64      !< X, Y, Z coordinates encoded in base64.
      !-------------------------------------------------------------------------


      fd%ni=size(x) ; fd%nj=size(y) ; fd%nk=size(z)

      write(buf1,'(I6)') fd%ni
      write(buf2,'(I6)') fd%nj
      write(buf3,'(I6)') fd%nk

      write(unit=fd%unit) ' <VTKFile type="'//'RectilinearGrid'//&
                  &'" version="0.1" format="'//trim(fd%format)//'" byte_order="' &
                  & //trim(fd%type%format%endian)//'Endian">'//newline
      write(unit=fd%unit) '  <RectilinearGrid WholeExtent="1 '//trim(adjustl(buf1))//&
                                 &' 1 '//trim(adjustl(buf2))//' 1 '//trim(adjustl(buf3))//'">'//newline
      fd%type%format%geom%XMLend='  </RectilinearGrid>'

      write(unit=fd%unit) '   <Piece Extent="1 '//trim(adjustl(buf1))//' 1 '//&
                             &trim(adjustl(buf2))//' 1 '//trim(adjustl(buf3))//'">'//newline
      write(unit=fd%unit) '    <Coordinates>'//newline

      write(unit=fd%unit) '     <DataArray type="Float32" Name="X_COORDINATES" format="binary">'//newline
      call pack_data(a1=[int(fd%ni*sr, di)], a2=real(X, sr) ,packed=XYZp)
      call b64_encode(n=XYZp,code=XYZ64)
      write(unit=fd%unit) '     ', XYZ64, newline
      write(unit=fd%unit) '     </DataArray>'//newline
      write(unit=fd%unit) '     <DataArray type="Float32" Name="Y_COORDINATES" format="binary">'//newline
      call pack_data(a1=[int(fd%nj*sr,di)], a2=real(Y, sr) ,packed=XYZp)
      call b64_encode(n=XYZp,code=XYZ64)
      write(unit=fd%unit) '      ', XYZ64, newline
      write(unit=fd%unit) '     </DataArray>'//newline
      write(unit=fd%unit) '     <DataArray type="Float32" Name="Z_COORDINATES" format="binary">'//newline
      call pack_data(a1=[int(fd%nk*sr,di)], a2=real(Z, sr) ,packed=XYZp)
      call b64_encode(n=XYZp,code=XYZ64)
      write(unit=fd%unit) '     ', XYZ64, newline
      write(unit=fd%unit) '     </DataArray>'//newline
      write(unit=fd%unit) '    </Coordinates>'//newline
      write(unit=fd%unit) '    <PointData>'//newline
      deallocate(XYZp)
      deallocate(XYZ64)
   end subroutine write_mesh_3d_XML_bin_rect_xyz

!---------------------------------------------------------------------------------------------------------------------
!        VTK_collect_file_gen
!---------------------------------------------------------------------------------------------------------------------
   subroutine VTK_collect_file_gen(fd, OutFolder)
      implicit none

      class(VTKfileHandler), intent(inout) :: fd
      logical, intent(in), optional :: OutFolder
      character(len=10) :: rank, snapshot
      character(len=80) :: f, vtrfile
      ! integer           :: code
      integer           :: shot, err, nt, np, k
      logical           :: file_opened
      logical :: outf
      !-------------------------------------------------------------------------


      if (present(OutFolder) ) then
         Outf=OutFolder
      else
         Outf=.false.
      endif

      !... Looking for a none connected logical file unit.
      if (iproc == 0) then
         fd%unit=99
         inquire(unit=fd%unit, opened=file_opened)
         do while (file_opened .and. fd%unit /= 0)
           fd%unit = fd%unit - 1
           inquire(unit=fd%unit, opened=file_opened)
         end do
         if (fd%unit == 0 .and. file_opened) then
           print *,"[WARNing]   ","VTK_open_file","warning, all file units from 0 to 99 are already connected."
         else
            if (outf .eqv. .true.) then
               np=scan(STRING=fd%prefix, SET="/", BACK=.true.)
               nt=len_trim(fd%prefix)
               f=trim(adjustl(fd%prefix(np+1:nt)))//".pvd"
            else
               f=trim(adjustl(fd%prefix))//".pvd"
            endif
           open(unit=fd%unit, file=trim(adjustl(f)), form="FORMATTED", status="replace", &
              action="write", iostat=err)
           if(err /= 0) print '("VTK_collect_file: Error, problem creating file ",a,".")', trim(f)
           write(unit=fd%unit,fmt='(100A)')   '<?xml version="1.0"?>'
           write(unit=fd%unit,fmt='(100A)')   '<VTKFile type="Collection" version="0.1" format="'//trim(fd%format)//'">'
           write(unit=fd%unit,fmt='(100A)')   '  <Collection>'
           nt=len_trim(fd%prefix)
           np=scan(STRING=fd%prefix, SET="/", BACK=.true.)
           if (outf .eqv. .true.) then
             np=scan(STRING=fd%prefix(1:np-1), SET="/", BACK=.true.)
           endif
           vtrfile=fd%prefix(np+1:nt)
           if ( nb_procs == 1 ) then

             do shot = 1, fd%counter
               write(snapshot, '(i6)') shot
               write(unit=fd%unit,fmt='(100A)') '    <DataSet timestep="'//trim(adjustl(snapshot))//&
                                         &'" part="0'//'" file="'//trim(adjustl(vtrfile))//&
                                         &"_"//trim(adjustl(snapshot))//trim(adjustl(fd%ext))//'"/>'
             end do

           else

             do k = 0, nb_procs-1
               write(rank, '(i6)') k
               do shot = 1, fd%counter
                 write(snapshot, '(i6)') shot
                 write(unit=fd%unit,fmt='(100A)') '    <DataSet timestep="'//trim(adjustl(snapshot))//&
                                           &'" part="'//trim(adjustl(rank))//'" file="'//&
                                           &trim(adjustl(vtrfile))//"_"//trim(adjustl(rank))//&
                                           &"_"//trim(adjustl(snapshot))//trim(adjustl(fd%ext))//'"/>'
               end do
             end do

           end if
           write(unit=fd%unit,fmt='(100A)')    '  </Collection>'
           write(unit=fd%unit,fmt='(100A)')    '</VTKFile>'
           close(unit=fd%unit)
         end if
      end if
      fd%counter=0 ; fd%restart=0 ; fd%first=.true. ; iproc=0 ; nb_procs=1
   end subroutine VTK_collect_file_gen

!---------------------------------------------------------------------------------------------------------------------
!        VTK_read_file
!---------------------------------------------------------------------------------------------------------------------
   subroutine VTK_read_file(fd)
      implicit none

      class(VTKfileHandler), intent(inout) :: fd


      open(newunit=fd%unit, file=fd%prefix//fd%ext         )
      PRINT*, ' SUBROUTINE to write when I will have time: work in progress'
      close(unit=fd%unit)

   endsubroutine VTK_read_file


end module vtk_IO
